// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'
import Customers from './components/Customers'
import About from './components/About'
import Add from './components/customers/Add'
import CustomerDetails from './components/CustomerDetails'
import Address from './components/customers/Address'
import Contact from './components/customers/Contact'
import Location from './components/customers/Location'
import Home from './components/home/Home'
import Flight from './components/customers/Flight'
import App from './App'

Vue.use(vueResource)
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {path:'/customers', component: Customers},
    {path:'/about', component: About},
    {path:'/add', component: Add},
    {path: '/customer/:id', component: CustomerDetails},
    {path: '/contact', component: Contact},
    {path: '/address', component: Address},
    {path: '/location', component: Location},
    {path: '/', component: Home},
    {path: '/flight', component: Flight}
  ]
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})